---
title: RESEARCH TRIP - SHENZHEN
period: 07-13 January 2019
date: 2018-12-23 12:00:00
layout: page
term: 1
published: true
---

*"The future is already here, only unevenly distributed." William Gibson.*

![]({{site.baseurl}}/researchtrip.jpg)

As part of the Master's in Design for Emerging Futures we have just returned from a learning journey in Shenzhen also known as the "Chinese Silicon Valley".

This name is due to the economic and technological development of recent years, making this city a worldwide attraction for people with ambition to quickly develop and implement new ideas based on technology.

You can find documentaries such as these one I'm sharing with you from Wired, or shorter reports that give us an overview of what is happening there.

  *[Shenzhen: The Silicon Valley of Hardware (Full Documentary) | Future Cities | WIRED]()
  <https://www.youtube.com/watch?v=SGJ5cZnoodY>*

From a personal point of view and based on the experience lived there this text shows some of the observations gathered.

Shenzhen is a city that has grown exponentially over the past 40 years. It has change from a fishing town to being the cradle of manufacturing. China opted for a strategic direction from government based on local technological development going from production "everything to 100" to production "Created in China".

There you can find some of the big corporations as for example, "Tencent", developer of the "WeChat" , an application I will talk about later, Huawei or IBM.

Accompanying in this direction new companies have been born and many of them in a few years have grown at a great speed. Its success is due to a new model of production or service based on a business model that adjusts to today's changes and allows us to transform the way we understood manufacturing.

![]({{site.baseurl}}/landscape1.jpg)

“91% of the electronic devices for the global 7 billion people are now being produced in one single city. Let’s ponder on this and let this sink in. 91% of global electronics are now made in one single city. It is an unprecedented concentration of production in human history. Five decades of outsourcing have built a global supply chain infrastructure that makes Shenzhen today.”`*1`

To better understand this topic we visited the following companies:

- **Szoil (Shenzhen open innovation lab):**  <https://www.szoil.org/>

> “Shenzhen Open Innovation Lab (SZOIL), established by SIDA and Maker Collider, is a space and platform for worldwide makers to communicate and cooperate.The lab dedicates in exploring the issues and developing solutions to connect the massive production ecosystem to small hardware startups so as to promote the international standing of Shenzhen in the development of digital intelligent hardware and manufacturing and build a future intelligent hardware Silicon Valley by combining new open source method and current manufacturing system in Shenzhen.”



- **Seeed studio**: One of the world bigger open source hardware company.  <https://www.seeed.cc/>

> “The IoT Hardware enabler. Whether you are a developer , technical supplier, or industry player, we provide products and services for your IoT needs.”



- **X.Factory:** <https://www.xfactory.io/>  

>  "x.factory is operated by Chaihuo Maker Space, Shenzhen’s first and leading maker space since 2011, and partnered with Seeed Studio, an open source hardware company that has been serving the global maker community since 2008.  

>  We do two things at x.factory –

>  **MAKE** – x.factory is an “open factory” with production-level equipment for in-house prototyping and small-batch production services, as well as co-working spaces to make your project.

>  **CONNECT** – x.factory helps members to connect to Shenzhen’s vast resources in supply chain, as well as industry and market opportunities in China."`*2`


SZOIL was born in 2015, Seeed in 2008 and X.Factory as part of Seeed, was born in 2011, none is more than 10 years old.

The Hardware ecosystem has generated the space to build companies with adapted business models that focus from the production and distribution of hardware for, from makers to companies focused on mass production. For example in the case of X.Factory, it was born in the form of "makerspace" with the aim of linking the makers with the industry. They work as a connector and already have collaborated with companies such as Samsung, Microsoft or Intel.

These are just a few examples of how new businesses are being born out of the government's effort to generate an ecosystem focused on the execution.

We find in Shenzhen a city that mixes investors, companies, talents, in a wave that sweeps the market and society.  It's reflected in the following data:

1. Between 1978 and 2018 China's Gross Domestic Product (GDP) went from US$150 billion to US$12.24 billion (according to UN figures).

2. Poverty rate in China (% of total research population)

   ![]({{site.baseurl}}/researchtrip1.jpg)

   At the same time China also dramatically increased its emissions and carbon footprint.

   ![]({{site.baseurl}}/researchtrip2.jpg)

Is social wellbeing inherent to the economic development? And is the economic development inherent to social development?

Regarding the first question there is data that shows that in the more developed countries there is less poverty, consequently more access to literacy and education and more opportunities and options. Even so, the external conditions for the well-being of citizens depend most of the time on the policies of governments and the factual powers that generate relations and govern justice, education, politics, economy and culture (among others).

This leads me to think that social development is born of a combination of forces than estrictly from the economic development.

In Shenzhen, the management is applied in a top down direction and it seems that citizens accept this orientation which also allows them to benefit with more resources and opportunities.

In my opinion I would say that the combination of these two forces is what makes the speed of implementation radical to other places. Now, do we know what the consequences of this are? Should we be worried?

This is where conversations about control and privacy, ethics in the development of technology, the risks and benefits of artificial intelligence, etc. are born.

For example: WeChat is an application that integrates something like whatsapp, facebook, Slack, Skype, Tinder, Über, Paypal, etc. Chinese version in the same app. In exchange for using this platform "for free", weChat, or rather Tencent, the WeChat management company, keeps your data and negotiates with it.

Without talking about the issue of what is done with this data, I would like to signal that the access to all this information gives a control (of which I am not sure if people are aware) and can be applied in a way that can repress people's freedoms. For example;

> "WeChat in China and WeChat in the rest of the world have important differences. The clearest ones have no relation to money. In order to be able to use in China any social network habitual in the day to day of the Spaniards, like Facebook, Whatsapp, Instagram, Twitter or any product of Google it is necessary to use a VPN. However, while using the VPN, WeChat stops working. In addition, if you want to send certain words or phrases heard in the street to criticize the government, the application itself prevents the recipient from receiving the message.`*3`

Is this a real scenario that George Orwell would pose?

Every day we find news in relation to these issues that generate questions to understand what the balance is.

**How can rigorousness or the need for in-depth study of the issues be combined with dialogue and rapid implementation?**

![]({{site.baseurl}}/landscape2.jpg)


## Cognitive systems in AI

----

The research topic of Silvia Ferrari, Gabor Mandoki, Oliver Jugging and mine was 'The cognitive system in AI'.

Our research into AI in China revolved around three keywords, three themes that recurred in our observations. We believe future developments in the field of AI will be very much influenceced by:

- Infrastructure: the ability to develop new ideas into tangible things with flexibility and speed of execution

- Data & trust : a seemingly general attitude towards data sharing that values trust more than individual protection

- Innovation: a mindset that prioritize improving existing ideas more than disrupting the system

In the enxt video our presentation for the reflection.

(https://vimeo.com/312961246)




------


[*1]: <https://medium.com/szoil/shenzhen-white-label-electronics-b4fe4322d589	"Szoil. Shenzhen white label electronics."
[*2]: https://www.xfactory.io/&gt;	"X.Factory"
[*3]: https://retina.elpais.com/retina/2018/01/15/tendencias/1516015128_267599.html
