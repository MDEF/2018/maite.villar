---
title: MATERIAL DRIVEN DESIGN
period: February 2019
date: 2019-03-20 12:00:00
layout: page
term: 1
published: true
---

## Material driven design
With Mette **Bak-Andersen** and **Thomas Duggan**

-----

The course took over four weeks; the first week was focused on introducing us to sustainable design theories and to the bases of “material driven design”(MDD in fordwards). Next week we had chosen a raw material to continuously experience the MDD method personally.

The Material driven design course has been structured by the next points.

Objective:
- Test a design process based on a material dialogue in the context of digital fabrication

Learning objectives:
- Understanding the principles of designing physical objects and structures for a circular economy
- Being able to identify and source of alternative biomass waste materials
- Being able to use a material driven design process for sustainable design
- Being able to use the material dialogue from craft in the context of digital design and fabrication.

Course process
- Introduction to sustainable design: theories
- Experiencing the material conversation: Introduction to the method
- Material driven design: Experiencing the method


## Introduction to sustainable design

-----

“All factor influencing a system can together be seen as a whole, but we can never understand any single part of it fully without attending to its connection to the whole.” - Andersen.

¿Qué te viene a la cabeza cuando te dicen “Sustainable design”? De esta pregunta salieron estas respuestas: Something that can be put back in the nature, designing for a loop life cycle, it’s sustainable design when the elements of the whole system are balanced, etc.

One of the main consensus of where do we find the difficulties for designing sustainable resides in the efficiency and the cost. What happens when the system itself difficultes it? Researchers and professionals has studied alternative ways of organizing into the system and suggest different processes and methods.

This work in looking for alternatives has bring us different frameworks and methods to face the actual linear system that is exploiting the natural world, transforming material and selling consequently generating a huge not degradable waste problem.

The already known “Circular economy” mainly leaded by Ellen Macarthur explains clearly what it is about
and the framework that presents for closing the loop.

https://www.ellenmacarthurfoundation.org/

**What is a circular economy?**

-----

Looking beyond the current take-make-waste extractive industrial model, a circular economy aims to redefine growth, focusing on positive society-wide benefits. It entails gradually decoupling economic activity from the consumption of finite resources, and designing waste out of the system. Underpinned by a transition to renewable energy sources, the circular model builds economic, natural, and social capital. It is based on three principles:
- Design out waste and pollution
- Keep products and materials in use
- Regenerate natural systems

![]({{site.baseurl}}/1_systemdiagram.jpg)

Over time, design companies have been outsourcing production to countries where production is cheaper, pushing a disconnection in the relationship between designer and material. The knowledge that a craftsman had when dealing with materials is being lost, so today there are design methods to learn from the experience with the material. This is concretely called "the material dialogue in crafts".

**Material dialogue in crafts**

----

The differences of the leanring approach are these;

- Knowing of  >< Knowing how
- Formal knowledge >< Tacit knowledge
- Design thinking >< Design doing/making
- Learning about >< Learning with (Apprenticeship)
- Interactional expertise >< Contributory expertise
- Online >< Offline cognition
- Conscious >< Unconscious (Automaticity)
- Mind >< Body

The learnings comes from the embodied cognition (Shapiro, 2010). This is equiparable to how do we learn when we practice “Learning by doing”.

Here I would like to add a reference of “The knowledge creation company” on how different phases helps to generate knowledge.  

![]({{site.baseurl}}/2_knowledgecreation.png)

## Experiencing the material conversation**

----

In “Material experience Lab” they present the material driven design as the next method:

Material Driven Design (MDD) method facilitates designing for material experiences. It suggests four main action steps presented in a sequential manner as:
(1) Understanding The Material: Technical and Experiential Characterization,
(2) Creating Materials Experience Vision,
(3) Manifesting Materials Experience Patterns,
(4) Designing Material/Product Concepts.

The process starts with a material (or a material proposal), and ends with a product and/or further developed material. The method emphasises the journey of a designer from tangible to abstract (i.e., from a material to a materials experience vision, illustrated with dashed lines and lighter colours in the bubble for Step 2), and then from abstract back to tangible (i.e., from a materials experience vision to physically manifested, further developed materials/products).

![]({{site.baseurl}}/3_materialdriven.jpg)

We have experienced this process in a intuitive way. I have take this graph from the Material experience lab web.

As introduction, we went to Valldaura, a house in the mountain near from Barcelona. There we have been introduced to the history and the ecosystem where we were. Once we had a walk around, we took some wood trunks.log

![]({{site.baseurl}}/valdaura.jpeg)

The aim was to make a spoon from each of the wood trunk.

After five hours of wood carving, we gathered with our results and shared the different phases and emotions that we experienced. I summarize the learnings of all the process in the final of the documentation.

The material conversation triangle -

![]({{site.baseurl}}/triangulo.png)

## Material driven design - Experience

----

The process followed for experiencing the material driven design with a raw material was:
1. Understanding the “Eggshell”
2. Experiencing the material

**Understanding the eggshell**

----

- The eggshell in the society
- Properties and chemical composition
- Aplications

The eggshell in the society

----

**In science:**
An egg has always been a great object of research in science. It has often been called the "magic food" because of its versatility and capabilities.

**Culture:**
- The egg qualifies the symbolism of the constant renewal of Nature, of the rebirth. This is the meaning of the famous Easter eggs.
- Mythology: Typically the cosmic egg symbolically represents a beginning of some kind.  The image of the cosmic egg is known in many mythologies; it appears in the Orphic Greek, in the Egyptian, in the Finnish, in the Buddhist, in the Japanese, in America and in all regions of the world where men wondered about the origin of life.
- The eggshell in instagram:

![]({{site.baseurl}}/4_instagram.jpg)

Properties and chemical composition

----

- Protein based aligned with mineral crystals.
- Mainly calcium in chemical compositions as “Carbonato cálcico” that its present in a 90%
- The calcium in the eggs comes from sedimentation, there are no cells in the birds that generate the calcium from the shell.
- Eggs with harder shells are more mineralized than those with weaker or fragile shells.
- Although quite hard, in fact it is a semi-permeable membrane that allows air and moisture to pass through some 17,000 pores that cover its surface.
- Inner and outer membranes have collagen and keratin
- The shell of a single egg provides about 2 grams of calcium (for comparison, a glass of orange juice has about 11 milligrams).
- The chalaza - the two filaments that join the yolk and clear - and the membrane that covers the yolk are rich in sialic acid. It is basic for the immune and nervous systems.
- It is used to develop drugs and enhance children's cognitive development.
ne pigment, protoporphyrin IX, gives shells a brown colour. This pigment is a precursor of haemoglobin, the oxygen-carrying compound found in blood. Other pigments, such as oocyanin which gives blue and green colours, are side-products from the formation of bile. White egg shells have an absence of pigment molecules.

Aplications

----

**Health issues**

- Reduce wrinkles: Freeze water mixed with crushed egg shell powder and gently massage the face with the resulting cube to reduce the appearance of wrinkles.
- Improve your bone health: Egg shells contain large amounts of calcium. All you have to do is crush them and add them to your meals, in soups, salads, creams, etc. You won't notice any different knowledge and you will add more calcium to your bones. (Journal of Clinical Diagnostic Research)
- Strengthen nails: If you add crushed egg shells to the enamel and then paint your nails with it, you will strengthen them. It is a perfect remedy for people with nail growth problems.
Nutritive Mask
- Mix the shell powder with the egg white, apply it to your face and let it dry before rinsing. You'll get a healthy mask and facial tightener.

**Garden**

The eggshell is composed of 98% calcium carbonate, which is a very important mineral nutrient for plants involved in the cellular development of plants also some plants such as tomatoes, peppers and aubergines are susceptible to apical rot, which is caused by calcium deficiency. As egg shells can be an important source of bacteria, it is ideal to boil them for about 10 minutes over a fire, which is why the hard shell is perfect for this. Now we are going to see 7 uses of egg shells in the orchard.(Huerto)

- Combat pests such as slugs and snails ( 10 remedies to combat snails and slugs )
- Fertilizer Rich in Calcium
- Extra Humus or compost contribution
- Egg shell tea ( Prepare fertilizer infusion of egg shell )
- Improve pH by reducing soil acidity
- Calcium contribution to hens or birds
- Biodegradable eggshell seedlings

**Waterproof material**

Chinese researchers create a waterproof material from egg shells. A sustainable discovery that would cut costs. The material repels water, but let's oil pass.

According to an article in the prestigious scientific journal Nature, Chinese researchers have used egg shells to create an impermeable material capable of resisting radiation, corrosive liquids and other chemicals.

- This material could be used for floor coverings.
- This material doesn't let water through, but oil does. This could be useful for some type of filter in occasions of environmental crisis or in drainage of waters. A promising, economic and sustainable advance.


References:
http://www.lahuertinadetoni.es/7-usos-de-la-cascara-de-huevo-en-el-huerto-o-jardin/
https://blogthinkbig.com/cascara-huevo-agua-impermeable
https://www.quo.es/ciencia/g69763/por-que-los-cientificos-creen-que-el-huevo-es-un-alimento-magico/
http://blog.arion-petfood.es/la-cascara-de-huevo-como-aporte-de-calcio-para-perros/
https://www.youtube.com/watch?v=f87ErWc7Qtk
https://materialdistrict.com/article/shell-homage-bioplastic-eggshells-nutshells/

## Experimenting the eggshell

----

![]({{site.baseurl}}/Eggsheel.JPG)

In the next presentation I show some of the recipes used when working with the eggshells.
I went from simple experiments as mixing the shell with elements as water or vinegar; to try to get a bioplastic and from other hand ceramic.

ISUU


## Learnings

----

Learnings:

- Any stupid idea that come to your mind do it.
- Come backguards to use the main material
- Understand the material.

You also understand yourself better: Where you have frictions and what makes you flow
- Don't know how to start
- Once you start more ways of continuing occurs
- See your behaviour. Working with someone it's much more effective.
- I struggle to take fotos and document the process scientifically and in detail.

Extrapolable to the project
- It’s never going to work in the first time.
- Face it, spend hour and hours and after will become something.
- Follow your intuition without nedding supervision all the time.
- Talk with experts
- Put the material in the paradigm they belong (Conections and networks) *The scientific method an production system has created a disconnection with its network of relationships*
- Work in different experiments in the same time. It's better to play with more cards and choose the best result to play with.
- Collaborate with people. Feed the process.
- Look at the things that stops you for going fordward. Reflect and find your strategies to face them. Extrapolate it to the project.
