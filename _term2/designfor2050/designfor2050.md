---
title: DESIGN FOR 2050
period: April 2019
date: 2019-04-20 12:00:00
layout: page
term: 1
published: true
---


**Syllabus and Learning Objectives**

*What futures do we want to remember by 2050? What stories do we need to imagine collectively today to be able to shape better tomorrows 1 billion seconds from now? In fact, what is a billion? How can a thought experiment as reimagining the meaning the internet(s) and thinking of it in plural, empower us and change the way we relate between humans, non-humans and with the Planet? What if we start thinking of ourselves as internet citizens rather than internet users?

In this course, participants will work in collaborative ways to design fictional narratives around the concept of internet citizenships and post-technological futures, integrating their final projects and research insights, using as a 1 billion second time horizon together with key insights from the research themes that IAM has been exploring in the last years, in particular from 2019’s theme: The Quantumness of Archipelagos, a ‘what if?’ remix of ideas coming from philosophy, geography, queer theory and quantum physics, shaped as an experimental thinking tool to deal with the complexity of our realities and a lens through which we can imagine alternatives, collectively.
The course will run as an experimental studio during 12 hours (split into 4 sessions), where participants will need self-organise to achieve a collective goal, refine the key questions shaping their projects, practice consensus, reflection, self-grading and shared learnings that will inform the individual projects and also the group performance. The main goal of this format is not only the quality of the final output but most importantly the collective learnings from running a studio, working in collaborative ways and developing interconnected mindsets around three main ideas: critical optimism, long-term strategies and planetary narratives*

Prismatic Minds
---

During ‘Designing with 2050’, we used the concept of Heterotopias to create an alternative Black Mirror with Andre / IAM under the name ‘Prismatic Minds’.

The overarching theme was Identity, the central object as narrative device is the Totem / Senses, and the main conflict is the Hyperreality vs Environment.

**Looking for Silence** with Emily, Veronica, Adri, and Kat.

---

![]({{site.baseurl}}/silence-01.jpg)
![]({{site.baseurl}}/silence-02.jpg)
![]({{site.baseurl}}/silence-03.jpg)
![]({{site.baseurl}}/silence-04.jpg)
![]({{site.baseurl}}/silence-05.jpg)
![]({{site.baseurl}}/silence-06.jpg)
![]({{site.baseurl}}/silence-07.jpg)
![]({{site.baseurl}}/silence-08.jpg)
![]({{site.baseurl}}/silence-09.jpg)
![]({{site.baseurl}}/silence-10.jpg)

**Script**

Jasmine slides her door shut, shuddering as she feels its vibrations pass through her hand, not another sense today please, she thought. Tiptoeing around her apartment, she avoids trying to look, feel or touch anything which will provide feedback. The smell of melted chocolate begins to drift through her apartment, a smell Jasmine used to adore from when she was a child. Fucks sake, the TV has turned itself on again, it heard her return. Piss off, she thought, feeling slightly revolted at the TV’s intrusion of her privacy.

Jasmine listens to the chaos outside, pondering over the consequences of her actions. Jakarta was already an eclectic mix of culture, all bundled together in a growing world with shrinking space. The senso-tattoo was supposed to be an intervention to make us understand the consequences of our actions, not become infatuated with them. Her bed begins to vibrate slowly, an attempt to ‘soothe’ her stress and anxiety sensing her increase in heart rate and temperature. Jasmine falls into a deep slumber.

–Break, screen goes black, music quiet, clock ticking sound.

A gentle breeze drifts through the streets, the advertisements are quiet today. Taking Balinese “Nyepi, day of silence” as an inspiration the 2050 world commemorated another day of silence, on the 9th March. On this day, most people stay in their homes. Only the strongest go out into the street to demonstrate. Except this demonstration remained quiet, a mass of bodies, standing, still, feeling the breeze upon their faces. Earth’s Day of Silence was a result of a saturation point in sensory noise, in which the huge population of Jakarta began to yearn for sensory hits, missing work, stalking chefs and other senso-tattoo developers to chase an elevated, unrealistic sense of reality and existence.

Jasmine was initially a part of the senso-tattoo movement, but saw sense with the realisation of what she had helped to create. The self-hacking movement was similar to the Slow Movements of the past, a campaign for mediation of humanity, against the constant trend of consuming more and more.

A period for self-reflection, the day of silence turned into a day of awareness, in which governors and citizens alike began to realise what it really felt like to touch, to smell, and see. The
The following morning, Jasmine woke up, glad at the feeling of being a little numb. As the day passed, this feeling continued. Curious, she thought.

Not so curious, thought the senses, they had used the day of silence as a day to escape, returning to the networks they were extracted from, far far away from the tattooed skins of humanity. They were glad for the trip away, they were overworked and tired.

**Video**
[**LINK TO THE VIDEO**](https://vimeo.com/337008751)
<iframe width="420" height="315" src="https://vimeo.com/337008751" frameborder="0" allowfullscreen></iframe>
