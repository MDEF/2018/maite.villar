---
title: 14. WILDCARE WEEK
period: 27-06 March 2019
date: 2018-10-07 12:00:00
term: 2
published: true
---

![]({{site.baseurl}}/welding.jpg)

**Task**
Try and document one of the three wildcare week proposed processes. The three proposals are: Wood blending, metal welding and clay printing. In my case I've choosen the **Metal weldin** because it's a thing that other way I wouldn't do it by my own.

*"Welding is a fabrication or sculptural process that joins materials, usually metals or thermoplastics, by using high heat to melt the parts together and allowing them to cool causing fusion. Welding is distinct from lower temperature metal-joining techniques such as brazing and soldering, which do not melt the base metal."*

The materials needed for the metal welding are:

- Security clothes: Helmet to protect your eyes, leather gloves and coating. (Try if the helmet is workin switching on a lighter).
- Welding machine. Electric. In this case:  [**POWERTEC by LincolnElectric**](https://www.lincolnelectric.com/es-es/Equipment/Pages/product.aspx?product=K14047-1(LincolnElectric_EU_Base))
- The two metal pieces.
- clamp.

![]({{site.baseurl}}/helmetsglovesandcoats.jpg)
A picture of the machine we have used:
![]({{site.baseurl}}/machine.jpeg)
The gun:
![]({{site.baseurl}}/gun.jpeg)
The negative claw:
![]({{site.baseurl}}/negativeclamp.jpeg)

Security first:
- Dress the coat, the gloves and the helmet.
- Each time one is going to start welding say loudly "Readyyyyyy!"

Metal welding step by step:

- Hold a metal piece with a clamp.
- Connect the negative current claw to the clamp. (The picture before).
- Switch on the gas.
- Look at the gun to see if the filament inside is too long or short. It should be 2 cm long more or less.
- If it's too long or it has a ball in the end, cut it.
- The gun has a button to push the filament out. If it's not touching the metal connected to the negative current nothing happens.
- Once you are ready say "Readyyyyyy" and wait to listen others respond.
- puch the button of the gun and slowly, making small circles you need to go filling the separation between the two metal pieces.

Learnings:

- If the gun's metal is too long the line it's going to be uniform and it's not going to join properly the two sides.
- If the speed is interrupted the same it's going to happen.
- The result should be a continuous homogeneous fat line:

![]({{site.baseurl}}/welding.jpeg)
