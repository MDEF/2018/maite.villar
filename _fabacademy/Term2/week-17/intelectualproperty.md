---
title: 14. WILDCARE WEEK
period: 27-06 March 2019
date: 2018-10-07 12:00:00
term: 2
published: true
---

**What is intellectual property?
intangible property that is the result of creativity, such as patents, copyrights, etc**

There are different kind of ways of setting the conditions for the use of that intangible element.

![]({{site.baseurl}}/property.jpg)

This are one of the most common one. Next a short definition of them:

- Trade secrets:
- Trade marcs:
- Copyrights:
- Patents:

Without getting deeper in this ones I want to focus in **Cretive commons**. As an interesting approach to defining those therms to share and use whatever you have create or done.

It's more than only that. Lawrence Lessig the chief of Creative Commons organization wrote a book called "Free culture". Lessig's main idea is to present a social democratic alternative that can offer the greatest possible consensus, led by copyleft. Copyleft is a legal practice that consists of the exercise of copyright with the aim of promoting the free use and distribution of a work, requiring that licensees preserve the same freedoms by distributing their copies and derivatives. Authors can apply a copyleft license to computer programs, works of art, texts or any type of creative work that is governed by copyright.

Its supporters propose it as an alternative and defense against the public restrictions normally incurred by publishers and the entertainment industry.
This legal practice is proposed as an alternative to digital piracy that arises as a way of confronting copyright; a legal practice that grants copyright for published or unpublished works of creation. Lessig proposes copyright as an imposed practice, with an archaic, unfair and restrictive model for a new society supported by the Internet and new technologies. And copyleft as a cybernetic revolution.

These transformations and these new currents have affected the majority of the fields provoking a change in the known systems. At the present time, the intensity and speed of changes in society have a great impact on the types of organizations, generating new ways of working and new demands. Understanding technology as a catalyst for these changes, the digital transformation affects all aspects of existing structures until today, thus affecting businesses as well.

It's way us as designer need to think wich are the conditions under wich we share the work.

Here the differences under Creative commons:

![]({{site.baseurl}}/cc.png)
![]({{site.baseurl}}/ccmodels.png)

In my case, in this first instalation that I'm working on, there is code that I have generated in "Arduino" and "processing".

The Arduino code is going to be open. I learnt introducing to electronics with Arduino and I have used open source codes to build mine, so following this community based project, my code is going to be open.

In the case of Processing the code is going to be shared as **CC BY-NC-SA**. Bsed on these conditions:  **non-comertial** use and **derivative must be licensed under identical terms**.

I'm going to use this code for the web, I want to make them accesible, but non to make it comercializable.

**Download the files**

----

- [Link to the files]({{site.baseurl}}/project-codes.rar)
