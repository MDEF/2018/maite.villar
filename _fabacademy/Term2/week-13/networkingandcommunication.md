---
title: 13. NETWORKING AND COMMUNICATION
period: 27-06 March 2019
date: 2018-10-07 12:00:00
term: 2
published: true
---

#THEORETICAL INPUTS
---

**Wired communication**

---

**Paralel board:**
Each line transmit bites. It's simple but implies a lot of cables. The more data you need the more cables it needs.
Nowdays it's not used. It can be used mainly for **music**

*Mitico cable que se le ponía a las impresoras.*

**Serial interfaces:**
These interface operate as one wire.


**types of communication:**

- **Asynchronous communication:**
Works without the support of an external clock
Timing - Software and hardware -(same)

- **Synchronous communication:**
Synchronous data interface always pairs its data line(s) with a clock signal, so all devices in a synchronous bus share a common clock.

For **programming** (as we need to be accurated in the timing) and not changing the time clock.
We use the **ISP (Synchronous communication).**


**Asynchronous communication**

----

**What are UART?**

Hardware supported ports. Ports dedicated to commnunicate. Pins that are only dedicated to communicate.
Attiny44 hasn't these pins, and it's why we use the ftdi to simulate with a library.
This pins are called: **RX / TX**
RX doen'st go with RX (It's only receive) Always the wires are swaped. **RX with TX.**


**V-USB**

The data lines requires 3.3V and for that reason we need to limit the voltage


**Synchronous communication**

----

**I2C**

Most of the digital sensors use it. SDA is the data line and the SDL ??
When you are sending the package of data of one sensor (with it's ID)
In the same time you can send embeded other package of data from other sensor (With it's own ID)

- Witx I2C we can connect all the arduinos in the same time (With a master controlling 200 Arduinos)

For communicating with I2C use the library <Wire>

**Libraries for arduino.** [Click here to access to the next code examples]( https://hackmd.io/s/B15x5dn9V)
- Example 1 Master as receiver: requesting information
- Example 2 Master as sender: sending information
- TinyWireS: Example (for an analog pressure sensor that sends data via I2C):


**SPI**

Master out - master in (One line to talk and another one to receive)
Reset pint - to start the communication
**Libraries for arduino.** [Click here to access to the next code examples]( https://hackmd.io/s/B15x5dn9V)
- Example, arduino as master, sending:


**OTA communication**

Frecuency espectrum:
High frecuency is better to transmit more information.
Low frequencies reach much more distance that high frecuency.

![]({{site.baseurl}}/frecuency-spectrum.png)

Depends of the application you cna use one type of frecunecy or other:
 - Farther - Low frequency
 - Nearer - High frecuency

Range vs. Power:
![]({{site.baseurl}}/radiation-inverse-square-law.jpg)


**HC08-COMMUNICATION**

**NRF24-Communication**

It's a transceiver. IT's a PCB antena. Has an embeded antena. There is no master slave, they all can do everything.
We can avoid the wifi. It's synchronous based communication, it talks to the SPI.  


---

**Bluetooth communication**
Is good to connect few devices. With bluetooth it can't have multiple masters but it can have multiple slaves.

**WIFI**

It can connect everything with internet.
The ESP could be the: Acces point / Server / Station

In this case is the station:
![]({{site.baseurl}}/oqbVBnz.png)
