---
title: 16. INTERFACE AND APLICATION PROGRAMMING
period: 27-06 March 2019
date: 2018-10-07 12:00:00
term: 2
published: true
---

Processing + Arduino + Capacitive sensor
---

This week I teamed up with  [**Veronica**](https://mdef.gitlab.io/veronica.tran/fab-academy/week-15/) to create an **interface on Processing with a capacitive sensor.**

Step one was to get the sensor working on Arduino using the following schematic that we found online.

![]({{site.baseurl}}/interface-1.jpg)

We used the Arduino Uno with a resistor, touchpad, and a red LED to be the indicator. We had to switch out and experiment with a few different resistors for us to receive the correct value strength.

![]({{site.baseurl}}/interface-2.jpg)
![]({{site.baseurl}}/interface-3.jpg)

We then installed the example library on Arduino and installed the ‘Capacitive Sensor’ Library

![]({{site.baseurl}}/interface-4.png)
![]({{site.baseurl}}/interface-5.png)

Successful reading of the sensor on the serial monitor

![]({{site.baseurl}}/interface-processing.png)

After confirming that the sensor works on the Arduino, we then read the values on Processing. To do so I have to import the Serial library that is already within the Processing software – Sketch > Import Library > Serial. Then we had to ensure that the serial readings are able to be collected by Processing by creating a variable, and then convert the string into a float variable. I also had to change the port number to 3 before it worked.
From there we made some incremental changes to the drawing after getting a square to change the colour depending on the touch.

![]({{site.baseurl}}/interface-6.png)
![]({{site.baseurl}}/interface-7.jpg)

We experimented with a few different Processing examples but decided it was best to continue with a single geometric shape, and then changed the square to a circle to make the jumps less abrupt. The diameter and hence size of the circle is dependant on the strength of the touch.

![]({{site.baseurl}}/interface-9.png)
![]({{site.baseurl}}/interface-11.mp4)

**Download all files**

---

- Code [Link to the file]({{site.baseurl}}/interfaces-code.zip)
