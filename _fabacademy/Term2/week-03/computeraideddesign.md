---
title: 03. COMPUTER AIDED DESIGN
period: 21-28 January 2019
date: 2018-10-07 12:00:00
term: 2
published: true
---

We have had support classes for the introduction to the following programs: Fusion 360, Rhino/Grasshopper and Blender and Openscad and Inkscape. I, as a beginner and relying on my inexperience in design so this week I have decided to do some exercises to experience the softwares.

- The first exercise deals with some geometries that I have drawn on paper and I have moved to Rhinoceros to make 3D designs.
- The second exercise deals with a basic design but a little more complex that requires mastering more commands in the software to get the result. Since we had the introduction to Autodesk 380 I wanted to do this exercise with this program but my computer has been blocked and the screen has been turned off continuously trying to work in Autodesk. Consequently, for now I have removed this program from my list.
- The following exercise I have done with the intention of introducing to parametric design with "Grasshopper". I have had three different results that I show later.
- Finally I have added a design of my idea of the final project that you can find in ["Final project"](



**Experimenting from idea to Rhinoceros**

------

Making a really simple geometrical form from Sketch-es to digital.

First from the top view, make the rectangles needed.

**Command: _Rectangle**
-*First corner of rectangle ( 3Point  Vertical  Center  AroundCurve  Rounded ): 0
- Other corner or length ( 3Point  Rounded ): 100
- Width. Press Enter to use length ( Rounded ): 100*

![]({{site.baseurl}}/Rectangle.png)
**Command: _SrfPt**
-*First corner of surface
- Second corner of surface
- Third corner of surface
- Fourth corner of surface*

![]({{site.baseurl}}/Surface.png)
**Command: ExtrudeSrf**
- *Select surfaces to Extrude
- Select surfaces to Extrude. Press Enter when done
- Select surfaces to Extrude. Press Enter when done
- Select surfaces to Extrude. Press Enter when done
- Select surfaces to Extrude. Press Enter when done
- Extrusion distance <5> ( Direction  BothSides=No  Solid=Yes  DeleteInput=No  ToBoundary  SplitAtTangents=No  SetBasePoint ): 100 // To make it a cube*


After I've continued doing the same process from the right view.

Based on, rectangle, Surface and extrude I've get to the result I wanted.
![]({{site.baseurl}}/Right view.png)
![]({{site.baseurl}}/Right view extrude.png)
To build the finish structure, I've change the layers of the construction lines to **#hide** them and **#group** the solid parts. Once this done, render it, and see the final result.

Is a very easy exercise even for people without any experience like me. The most interesting thing about the exercise is that you can try to imagine a geometry in a paper and translate it into digital.
![]({{site.baseurl}}/Final.png)
![]({{site.baseurl}}/FA_W2_Geometry1.jpg)

**Experimenting from idea to Rhinoceros (2)**

------

In this geometry there is no extruding. The unique thing needed to take in account to design it is to be aware that the surfaces are in the right direction. For that, put the view in "Shaded" mode and click in the surface to verify with **_ShowDir**. If it's in the wrong direction **_flipSrfc**. Easy.
![]({{site.baseurl}}/geometry2.jpg)

**Microwave in Rhinoceros**

------



I have recorded the process of the microwave design with a fermium kind of software. The result have been a really nice video with a huge water mark in the middle. Horrible. The lesson is, document the process with "imp pant" and try the softwares with which you are going to experiment with a small example before using it.

Anyways, I show here the result and the link to the Rhino file.
![]({{site.baseurl}}/FA_w2_Design1.jpg)

**Grasshopper - Exercise 1. Parametric pattern**

------

For my first contact with grasshopper I follow a on-line tutorial.

With **double click** a blank bar appears to introduce the function you need. First thing I have done create a **Hexagon grid**, connecting to numerical bars to manipulate the **size** and the **quantity** in Y and X directions.

The **Center** is connected to an **area** that connected to the **Circle** generates the circle inside of each hexagon.  In the other side, The point are connected to a **distribution** parameter, that **divide** s the points with the aim to generate this progressive pattern from smaller to bigger.
![]({{site.baseurl}}/w2_Parametric1.png)

**Grasshopper - Exercise 2. Voronoi.**

------


The main function of these pattern it's called **Voronoi**.  This functions divides a **set of points** and **changes the position** of those in a specific region or **boundary**.

This parameter is divided between **point**s, **radius**, **Boundary**,**plane** and **cells**.

First I've generated a **PlaneSurface**, with the X and Y number bars. This is connected to 3 things: Voronoi, PopGeo and EvalSrf.

First, **EvalSurf** works to manipulate the area where you want change something as adding points. Its possible to generate areas in different ways: Letters, Geometries, whatever form that works as a boundary. In this case I've used the **MD slider** that makes the parameter fro 0 to 1.  Connect this to **EvalSrfc** to select an area and add more **point**s. In this case the area is generated in a **circle** form. Connect with **Boundary**, and the same way as in the first connection, link it with **PopGeo**. Population Geometry works to set a number of points. To modify the set add counter bars. Finally one a set of point is defined inside an area and we can play adding more points in a specify are on it, connect it to **Voronoi** point link. The boundary is linked to the first **Surface** to affect to everything inside that.

Add **explode** and **nurbs** and play.
![]({{site.baseurl}}/parametric.png)

This is useful to understand how **Voronoi** works. The next exercise is focused on using this pattern in a specific area to achieve different results.


**Grasshopper - Exercise 2. Parametric doors design**

------

As in the example I put before, this exercise is made with a **surface** that its connected to **Pop2d** that works to set a number of points too. Number slides to manipulate the quantity of point and the seed. A useful tip here is that when adding the surface if clicking with the right button of the mouse we can set a specific area or multiple area as surface.

The main difference in this example is the **Neg** function that generates the area the way around, solid where the hall was and a hall where solid was. Connecting this with the **offset** we can easily change the patterns in the door.

![]({{site.baseurl}}/voronoidoors.png)

Result:
![]({{site.baseurl}}/FA_w2_Parametric2.jpg)

**Final project: Smart mirror**

------

Although it is not my final machine my next goal for the next to weeks is to build a "Smart Mirror". For this I drew a sketch of what is necessary and digitized it with the intention of being able to make use of it when I have to cut the wood of the frame.

Here is the render of the design
![]({{site.baseurl}}/FA_w2_Finalproject.png)

Files

------

- Final project file: Rhinoceros [Link to the file]({{site.baseurl}}/resources/Finalproject.3dm)
