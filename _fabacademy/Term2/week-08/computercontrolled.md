---
title: 08. COMPUTER CONTROLED MACHINING
period: 06-03 March 2019
date: 2018-10-07 12:00:00
term: 2
published: true
---


This week assignment is to do "something big" using the CNC.

- The computer controled machining it's a substractive procedure
- It's used to produce durable and strong partners
- Most of the times we use homogeneous material. As it is homogeneous the piece is going to be strong, so it will have the same characteristic in all the piece.

The materials can be used are these:

- Steel
- Aluminium - Hard / easy to the machine /
- Mdf - Very uniform / cheap / Difficult to make good srfc finish
- Woods
- Plastics - Clear / brittle
- Foams - Good for large objects / have to choose correctly glue or coating
- Wax - Very soft / melts easy while machining / easy breaks
- Machinable wax - smooth srfc finish - soft / Expensive

The material we've used is **Playwood**.

I have never use this machine, so the process we have followed is:
1. Make the design
2. Cut in the CNC


**Making the design**

----

Design tips
Scale : Millimeters
Clean: Not duplicate / Closed lines / 0.0 origin / No random points
Different files for different materials
Explode the text: Create outlines for all text
Thin lines: 0.2 thinkness

CNC wood joinery:
Temrinology: goove
Rabbet
Tenon
Half blind joint
Through joint
…

KERF - The stronger the endmill is the less kerf you have
CAM basics
Keep tool diameter in mind
Final cutting depth
Think about holding your shape
Make a plan
Think two ice

When doing the design the first proposal to cut was the next. A table made by 8 legs:
![]({{site.baseurl}}/CNC_diseño1.1.png)

To join the legs between them we designed a detailed design:
![]({{site.baseurl}}/CNC_diseño1joints.png)

When I asked for feedback to cut this files in the CNC I had this main answers:
- The CNC only cuts in three axes. It can be a simple design but complex for the CNC.
- You need to start from something simple for the machine.
- Once you understand the machine you can make two side, more detailed or complex designs.

So, after this feedback, the time we spent and everything to be done, we made some simple selfs that could be use for my room.
![]({{site.baseurl}}/Shelfs.png)

## Cutting with the CNC

---

This assignment have been done with **Julia Bertolaso**. Here to access to her documentation:[Link to the page](https://mdef.gitlab.io/julia.danae/fabacademy/week7/)

**Basics for CNC milling**

CNC machine in the Fab Lab is a three axes machine. It is possible however to have machines with five or six axes allowing more movement and intricate details to be cut by a flexibility in changing of position.

Different types of cuts Able to flip around your piece to mill on different surfaces of the material used. Usually used to produce big scale, durable and strong parts. What you can cut depends on the skill and the strength of the machine (which settings to use). Stacking different parts can save a lot of materials and energy so separate the parts to then join them yourself and maybe have the CNC mill again different parts. When prototyping small pieces, account for time, money and wasted materials when using of the machine. It is better to start with 3D print or laser cutting tests. Learn to prototype for testing functionality and aesthetics, it is more forgiving.

Foam good for testing milling and create a mould. If it fits properly you can then use another more expensive materials such as wood. Be strategic for preparing file and cutting methods to save time and money.

*Design tools*
Sketch Chair open source software to design and test functionality of object.
FlatFab parametric design tool.
Rhino and RhinoCAM is the program I will be using to design and to set up the file for the machine.
Router is similar to DC motor but doesn’t have much strength. Spindle is more expensive, more silent, faster. The collet is what is holding is used to hold bit in the milling machine. Always use one that is the size of your mill.
Geometries of end mills A flute is the cutting part of the end mill. The amount of flute on a end mil can vary, the more flutes the better finish. The CNC always works clock wise. By knowing this you can understand what end mill does by its geometry. Straight, up, down and compression (tong cut) flutes are the standard ones. Choosing the right one allows you to plan where the chips is going to go when cutting and avoid it to clog with materials. You want to get rid of chips as fast as possible. If I create dust and not chips, it means I am not cutting correctly my material. Drill bits vs end mills_ Carbon reinforced steel used for end mills. Shorter than a drill bits and has different grooves. End mill has bigger shank and smaller cut length than a drill bit. The thickness of the material you want to cut always needs to be smaller that the cut length of the overall tool length. The cutting edge of a end mill, also called tooth, is going inward or flat rather than a point for drill bits. Other flute mill types_ Single or O flute mill for plastic like HDPE and acrylic. (round) 60 or 90 v-bit for cutting hardwood signs. (diamond)
Ball nose cutters - Spoil board cutters - cut a lot of flat materials and even out surface really fast.
Materials Depending on the materials you want a different movement for milling. Fix material securely onto the surface to limit the risk of having the end mill pulling up the material… Compression best for plywood. Up cut best for metals or hard woods. Straight best for MDF or plastics. Down cut for general soft wood.
Listen to the machine working. Shattering happens when there is a micro vibration when cutting, to change this sound I can adjust how deep I have set my end mill in the machine. If the cutting is done too slow, therefore doesn’t extract enough chips, it is making a much high pitched noise, which I want to avoid. In the opposite, if the end mill is forced to eat too much material at once it will make a low pitched sound and might break the end mill.
Feed rate/speeds golden rules Chip load equation/calculation 0.15 chip load equal feed rate divided by rpm (max 24000) times 2. Look for chip load on charts online because it depends on material, machine and tools using. Calculate top and minimum…
VCarve software to create the path Always in millimetres 1:1 scale. Difference between inside cut, on line cut or outside cut. Pockets first, in cut and then out cut.
CNC Step BCN is the machine used in the fab lab, export .nc code.


**Design joint for CNC**

We wanted to use the precision of the CNC to experiment with wood joinery. However it seems this might be too complicated and be time consuming. Therefore we have designed something else to learn the process but would still like to try to make a wood joint just to see what kind of knowledge we need to have along with how precise I can get through several tests.

Based on our first design idea we cut the joints.

![]({{site.baseurl}}/rendertableandjoint.jpg)
![]({{site.baseurl}}/wireframejoint.jpg)

to make precision joints requires a good knowledge on the choice of tools and the strategies used to prepare the file, but it also requires several attempts to reach a level of perfection.

Files set up for hard wood joinery RhinoCAM is a plugin of Rhino software used to prepare the files and export the G code for the CNC to work. First, I fill in the STOCK, which is the piece of material - in the case of the joint hard wood - I will be using. Coordinates to 0,0,0. I select the top left corner as of where the coordinates will refer to. Usually for wood we put at the top corner but for metals it would probably be the bottom left corner set up. And I add the dimensions of the board, with 42mm thickness, 100mm x and 120mm y, for this wood, and we have designed the joint with exactly this thickness too. On the top surface, we then place a rectangle of the same dimension of my material and create an offset of 2cm inside. We make sure to position my design in the right place and depth. We then choose the strategies we will be using to cut the joint out of the wood board. First we will use the cutting strategy to cut out the pieces but leaving bridges we will set up later. To do this we made a two dimensional trace of the two pieces.

![]({{site.baseurl}}/RhinoCAMJoint.jpg)

In 3 axis advanced, we select horizontal roughing as my second strategy set up to mill the parts of material that is not needed for this design. It will clean up for the second thinner tool to do the other strategies. Now the we do the mesh outline by clicking to select curves (2D traces) and create an offset of 4mm. After we select the curves and go to control parameters > down and cut levels. Select the tool > 30 45 6(shank diameter) and 35 32 30, but those settings might have to change after seeing the results of the simulation. We the define my tool in more details along with the speed and cut (which is how quick x, y). For now we have put a flat 6mm down cut end mill. Plunge 3000 3000 3000 speed 18000 RPM and cut 5000. This setting is the average setting but it will probably change when we will be sure of the tools we will use and make the chip load calculation. Transfer set 1000. Next, we make clearance plane > stock maximum 2 distance > 10 ( but if there’s screws, we would put it higher). we simulate all of the strategies > to end and check if right tool is used. Program > machine operation > 3 axis advanced > parallel finishing (next strategy) > select curve (same as before) then sorting minimum distance. Some strategies will have the small tool such as the engraving for screws and holes for bones and then we will change too bigger tool of 10 or 8 diameter. We need to make sure the flute is long enough for it to cut through my wood. Select both and simulate both strategies. We go change the angle cut of the parallel finishing to 90. We had to go back and change the angled parts inside the joint to make it more simple otherwise we would have to find a way to flip one of the pieces in the middle of the whole process so that it could mill the angle on the other side. We changed them to straight 90 and 45 degrees angles. We added two bones for the corners of the inside of the joint to let it slide completely.

![]({{site.baseurl}}/mills.png)

We go back to the tools to check for their availability in the Fab Lab. They require a minimum length of 50mm and thickness depends on its speed for rotation. The thinner the quicker. For the 4 strategies, I first use a 4mm tool (We set up the distance it does at once to mill down of half the number of diameter of the tool, so in this case 2), then a tool of 10mm for roughing, the 4mm for finishing and then 10mm again for profiling. Not forget to change the fits and speed for tools again.

Chip load calculation we do the chip load calculation with the help of online website that show a max and min cpt for the corresponding tool. So the calculation is feed = cpt x number of flutes x RPM. In average for soft plywood and a 6mm tool we put 18000 RPM and have a 5000 feed. The minimum speed of this CNC milling is 18000 RPM and maximum 24000 RPM.

Here we will be using a 4mm which is about 0.15 cpt and the tool has one flute with a speed of 18000 for a feed of 2700. The second tool is 10mm with a cpt of 0.4, one flute and speed of 18000 with a feed of 7200. It seemed a bit too high so I tricked the machine by putting 6000 as the 100%.

![]({{site.baseurl}}/software.png)
![]({{site.baseurl}}/joints.png)


## Design shelf for CNC

---

![]({{site.baseurl}}/RenderedShelf.jpg)

For cutting the shelfs the process was this:

File set up shelf in plywood. First had to correct the space where the other board would slide in, from 15.47 mm to add and offset of 0.25mm to make it fit better. I add H bones (so that we don’t see them when pieces combined) with circles of 6mm diameter (min) to give enough space between pieces for bridges. I set up points for the screws and make 15mm circle. 40mm is required in between when screws are in between. Engraving > select curve > 3mm depth engraving for screws set up. I do the profiling, use outside, cut depth 16mm.
Put Entry/Exit > no and no. I place 5 5 4 rectangular bridges. Once both design files are ready I make sure it is set up with CNC - STEP - BCN (the machine in the Fab Lab) and right click to strategy > post (which will generate the G codes) each strategy at a time as I will have to change the tools in between and set up the screws.

The choice between T bone fillet and dog bone fillet will depend on the model. It is chosen to be positioned in a way that will not be seen (if possible) once the pieces assembled. Always make the whole a little bigger that the size of your end mill. So here I will make a hole of 15mm, which is a bit more than double of the size of the tool I will be using, a 6 millimetre end mill.

![]({{site.baseurl}}/selve1.jpg)

CNC-STEP-BCN Once I have the G code files for each strategies, I make sure to set up the X and Y to 0 and screw the right collet for the correct end mill to go in and screw those onto the machine, Then I set up the Z. To do this I place the tip of the machine a little bit inwards so that I can go manually down close to the board and leave enough space to place the small round button tool and position it under. I got to custom and select the button with the little symbol and it will automatically stop when it touches the button.

![]({{site.baseurl}}/selve2.jpg)
![]({{site.baseurl}}/selve3.jpg)

Conclusion we have seen that the CNC machine along with using the right tools and methods can produce an excellent job to make wood joinery however when it comes to more intricate, such as the Japanese wood joinery with several angles, it would require a different process of making that would require more time and precision to produce the same result with a more complex design.

![]({{site.baseurl}}/japaness.png)

**Assembled shelfs**

![]({{site.baseurl}}/w8_shelf_finished.jpeg)
![]({{site.baseurl}}/w8_shelf_finished1.jpeg)

**Access to the Files**

------

- Shelfs Rhino File: Rhinoceros 6 [Link to the file]({{site.baseurl}}/FA_W8_CNC_Shelfs.3dm)
