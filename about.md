---
layout: page
title: About
permalink: /about/
---

I come from a career based on "learning by doing" called LEINN (Leadership, entrepreneurship and innovation). Learning from mistakes and from the constante feedback, we have worked on the skills and weaknesses of oneself focused on personal development, the development of teams and work on projects focused on the creation of a company. This method of working let as experiment from the most conceptual part of defining an idea to the execution of the project. The four years of experimentation and the experience acquired during the end-of-career project in a innovation consultancy agency called "Ideas for Change", has reaffirmed a thought that deals with the importance of the team work and the creative thinking aimed at organizing the society in a way that each of us contributes to it.

After 4 years I have begun the degree with the aim to design an alternative way of living in co-existence, I've choose to continuie my life's learning and experimenting journey in MDEF (Master in design of emergint futures). The degree has been a gym for playing real and face the real challenges that exist in the market. Now, I'm focused on aligning myself, mind, body and actions to fix my purpouse in life, start from little and envision a possible and desirable better futures.

For that, each week of this master will help to get new imputs, generate new knowledge, connect thougths and reflect on them traveling between the most abstract concepts and the physical or material things.

Maite Villar.
